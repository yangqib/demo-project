module.exports = {
    extends: [
        'stylelint-config-recess-order',
        'stylelint-config-standard',
        'stylelint-config-standard-scss',
        'stylelint-config-recommended-vue/scss'
    ],
    rules: {
        indentation: 'tab',
        'selector-pseudo-element-no-unknown': [
            true,
            {
                ignorePseudoElements: ['v-deep']
            }
        ],
        'declaration-block-trailing-semicolon': null,
        'max-nesting-depth': 4,
        'no-duplicate-selectors': true,
        'no-invalid-double-slash-comments': null,
        'number-leading-zero': 'never',
        'no-descending-specificity': null,
        'font-family-no-missing-generic-family-keyword': null,
        'selector-type-no-unknown': null,
        'at-rule-no-unknown': null,
        'no-duplicate-selectors': null,
        'no-empty-source': null,
        'selector-pseudo-class-no-unknown': [true, { ignorePseudoClasses: ['global'] }]
    },
    ignoreFiles: ['src/assets/icons/*.css', 'src/assets/icons/*.html']
};
