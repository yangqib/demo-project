import { createRouter, createWebHashHistory } from 'vue-router';

const router = createRouter({
    history: createWebHashHistory(),
    // 严格检查路径末尾是否有尾部斜线（/）。默认为 false，意味着默认情况下，路由 /users 同时匹配 /users 和 /users/。
    strict: true,
    routes: [{}]
});

export default router;
