import axios from 'axios';
import { REQUEST_INTERCEPTORS, RESPONSE_INTERCEPTORS } from './interceptor';

axios.defaults.withCredentials = false;
axios.defaults.baseURL = '';
axios.defaults.timeout = 180000; // 超时时间30秒
// axios.defaults.adapter = require('axios/lib/adapters/http');

// 请求数据拦截处理
axios.interceptors.request.use(REQUEST_INTERCEPTORS, (error: { response: any }) =>
    Promise.reject(error.response)
);

// 返回数据拦截处理
axios.interceptors.response.use(RESPONSE_INTERCEPTORS, (error: { response: any }) => {
    return Promise.reject(error.response);
});

export default axios;
