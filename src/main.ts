import { createApp } from 'vue'; // getCurrentInstance ComponentInternalInstance
import App from './App.vue';
import router from './router';

import '@/styles/reset.scss';

import pinia from '@/store/index';

// 创建vue实例
const app = createApp(App);

app.use(pinia);
app.use(router);

// 挂载实例
app.mount('#app');
