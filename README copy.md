node 使用版本 16.10.0

    	"dev": "vite",

        "build": "vue-tsc --noEmit && vite build",

        "docs:dev": "vitepress dev docs --host 0.0.0.0",

        "docs:build": "vitepress build docs",

        "docs:serve": "vitepress serve docs",

        "clean": "rimraf node_modules",

        "preview": "vite preview",

        "eslint:comment": "使用 ESLint 检查并自动修复 src 目录下所有扩展名为 .js 和 .vue 的文件",

        "eslint": "eslint --ext .js,.vue --ignore-path .gitignore --fix src",

        "prettier:comment": "自动格式化当前目录下的所有文件",

        "prettier": "prettier .  --write",

        "commit:comment": "引导设置规范化的提交信息",

        "commit": "git-cz",

        "lint-css": "stylelint src/**/*.{html,vue,css,sass,scss} --fix",

        "preinstall": "npx only-allow pnpm"、

项目初始化
