export default {
    title: '项目文档',
    description: '前端规范、集成工具包文档',
    themeConfig: {
        nav: [],
        sidebar: [
            {
                text: '目录结构',
                items: []
            },
            {
                text: '开发说明',
                items: []
            },
            {
                text: '公共函数',
                items: []
            },
            {
                text: '公共组件',
                items: []
            }
        ]
    }
};
