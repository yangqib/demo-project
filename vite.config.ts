import vue from '@vitejs/plugin-vue';
import * as path from 'path';

import svgLoader from 'vite-svg-loader';
import AutoImport from 'unplugin-auto-import/vite';
import { createStyleImportPlugin, VxeTableResolve } from 'vite-plugin-style-import';

import DefineOptions from 'unplugin-vue-define-options/vite';

import removeConsole from 'vite-plugin-remove-console';

import { UserConfigExport, ConfigEnv } from 'vite';
// import { getPluginsList } from './plugins/plugins';

// https://vitejs.dev/config/
export default ({}: ConfigEnv): UserConfigExport => ({
    plugins: [
        vue(),
        AutoImport({
            // 自动导入 Vue 相关函数，如：ref, reactive, toRef 等
            imports: ['vue']
        }),
        removeConsole(),
        DefineOptions(),
        createStyleImportPlugin({
            resolves: [VxeTableResolve()]
        }),
        // svg组件化支持
        svgLoader()
        // getPluginsList(command)
    ],
    resolve: {
        //设置别名
        alias: {
            '@': path.resolve(__dirname, 'src'),
            '*': path.resolve(__dirname, './')
        }
    },
    build: {
        minify: 'esbuild'
    },
    esbuild: {
        drop: ['debugger']
    },
    css: {
        // css预处理器
        preprocessorOptions: {
            scss: {
                charset: false,
                additionalData:
                    '@use "./src/styles/variable.scss"; @use "./src/styles/mixed.scss"; @use "/src/styles/function.scss";@use "/src/styles/public.scss";'
            }
        }
    },
    server: {
        port: 8080, //启动端口
        hmr: {
            host: 'localhost',
            port: 8080
        },
        // 设置代理
        proxy: {
            '/api': {
                target: 'your https address',
                changeOrigin: true,
                rewrite: (path: string) => path.replace(/^\/api/, '')
            }
        }
    }
});
